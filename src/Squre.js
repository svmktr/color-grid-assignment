import React, { useEffect, useState } from 'react';
import './Square.css'; // Import the CSS file for styling

function SquareGenerator({ width, height, numColors }) {
  const [grid, setGrid] = useState([]);
  const [largestArea, setLargestArea] = useState({ color: null, count: 0 });
  let randomColors = []

  const generateRandomColor = () => {
    const letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  };



  for (let i = 0; i < numColors; i++) {
    randomColors.push(generateRandomColor())
  }



  useEffect(() => {
    const generateGrid = () => {
      const newGrid = [];
      for (let i = 0; i < height; i++) {
        const row = [];
        for (let j = 0; j < width; j++) {
          row.push(randomColors[(Math.floor(Math.random() * randomColors.length))]);
        }
        newGrid.push(row);
      }
      setGrid(newGrid);
    };

    generateGrid();
  }, [width, height]);


  useEffect(() => {
    const findLargestArea = () => {
      if (!grid || grid.length === 0) {
        return;
      }

      const directions = [
        [1, 0], // right
        [0, 1], // down
        [-1, 0], // left
        [0, -1], // up
      ];

      const visited = Array.from({ length: height }, () => Array(width).fill(false));

      let maxCount = 0;
      let maxColor = null;

      const bfs = (startRow, startCol) => {
        const color = grid[startRow][startCol];
        let count = 0;
        const queue = [[startRow, startCol]];

        while (queue.length > 0) {
          const [row, col] = queue.shift();
          if (visited[row][col]) continue;

          visited[row][col] = true;
          count++;

          if (count > maxCount) {
            maxCount = count;
            maxColor = color;
          }

          for (const [dx, dy] of directions) {
            const newRow = row + dx;
            const newCol = col + dy;

            if (
              newRow >= 0 &&
              newRow < height &&
              newCol >= 0 &&
              newCol < width &&
              grid[newRow][newCol] === color &&
              !visited[newRow][newCol]
            ) {
              queue.push([newRow, newCol]);
            }
          }
        }
      };

      for (let i = 0; i < height; i++) {
        for (let j = 0; j < width; j++) {
          if (!visited[i][j]) {
            bfs(i, j);
          }
        }
      }

      setLargestArea({ color: maxColor, count: maxCount });
    };

    findLargestArea();
  }, [grid, height, width]);


  return (
    <div className="square">
      {grid.map((row, rowIndex) => (
        <div key={`row-${rowIndex}`} className="row">
          {row.map((color, colIndex) => (
            <div
              key={`cell-${rowIndex}-${colIndex}`}
              className="cell"
              style={{ backgroundColor: color }}
            />
          ))}
        </div>
      ))}
      <div className="largest-area">
        <p>Largest Adjacent Area Color: {largestArea.color}</p>
        <p>Largest Adjacent Area Cell Count: {largestArea.count}</p>
      </div>
    </div>
  );
}

export default SquareGenerator;
