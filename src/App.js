import './App.css';
import SquareGenerator from './Squre'
import { useState } from 'react';

function App() {

  const [width, setWidth] = useState(null)
  const [height, setHeight] = useState(null)
  const [numColors, setNumColors] = useState(null)


  return (
    <div className="App">
      <div style={{ padding: 10 }}>
        <div style={{ padding: 5 }}>
          <label>Width</label>
          <input value={width} onChange={(e) => setWidth(e.target.value)} />
        </div>
        <div style={{ padding: 5 }}>
          <label>Hight</label>
          <input value={height} onChange={(e) => setHeight(e.target.value)} />
        </div>
        <div style={{ padding: 5 }}>
          <label>Colors</label>
          <input value={numColors} onChange={(e) => setNumColors(e.target.value)} />
        </div>
      </div>
      <header className="App-header">
        {width && height && numColors && <SquareGenerator width={width} height={height} numColors={numColors} />}
      </header>
    </div>
  );
}

export default App;
